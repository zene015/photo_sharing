<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}" type="text/css">
    </head>
    <body>
        {{--    コンポーネントの配置--}}
        <div id="app">
            <hello-world-component></hello-world-component>
        </div>

        <scipt src="{{ mix('js/app.js') }}"></scipt>
    </body>
</html>
